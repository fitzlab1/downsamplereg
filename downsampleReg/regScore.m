function [ score ] = regScore( imgStack )
% Scores how good the registration of a stack was.

maxImg = max(imgStack,[],3);

score = sum(sum(maxImg));

end

