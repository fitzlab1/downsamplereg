function [ output_images, reg_offsets ] = register( input_images, template )
% Performs image registration via downsampling + maximized correlation.
% Returns output images as well as their offsets from the inputs.

% === Parameters === %

%Downsample rates: As described in algorithm. Examples follow.
downsampleRates = [1/4, 1/2, 1];
%
%downsampleRates = [1/4, 1/2, 1] is the default. 
%
%downsampleRates = [1/16] will do a registration _just_ based on the 16x
%downsampled image. It will run pretty quick, but the output will only be
%accurate to +/- 8 pixels; it won't do fine motion correction.

%e.g, if this is at 1/4 and your image is 512x512, this will fix movements 
%of up to 128 pixels.
%Makes runtime of the first iteration a little shorter to set this lower.


[sizeY sizeX depth] = size(input_images);
reg_offsets = zeros(depth,2); %stores the output of the registration (Y,X format)

%Use normxcorr2 on most downsampled (smallest) image
r=1;
disp(['  registering images, iteration ' num2str(r)]);
downSizeY = sizeY*downsampleRates(r);
downSizeX = sizeX*downsampleRates(r);
templateImg = imresize(template, [downSizeY,downSizeX],'bilinear');
parfor d=1:depth
    regImg = imresize(input_images(:,:,d), [downSizeY,downSizeX],'bilinear');
    
    cc = normxcorr2(regImg, templateImg);
    [max_cc, imax] = max(abs(cc(:)));
    [yPeak, xPeak] = ind2sub(size(cc), imax(1));
    corr_offset = [(yPeak-size(templateImg,1)), (xPeak-size(templateImg,2))];

    reg_offsets(d,:) = [(yPeak-size(templateImg,1)), (xPeak-size(templateImg,2))] ./ downsampleRates(r);
end

%Use corr2max on successive pyramid levels
for r=2:length(downsampleRates)
    disp(['  registering images, iteration ' num2str(r)]);
    
    downSizeY = sizeY*downsampleRates(r);
    downSizeX = sizeX*downsampleRates(r);
    templateImg = imresize(template, [downSizeY,downSizeX],'bilinear');
    
    regOffsetsResult = zeros(size(reg_offsets));
    parfor d=1:depth
        regImg = imresize(input_images(:,:,d), [downSizeY,downSizeX],'bilinear');
        
        %refine previous offset
        minOffsetY = reg_offsets(d,1)*downsampleRates(r) - downsampleRates(r)/downsampleRates(r-1)/2;
        maxOffsetY = reg_offsets(d,1)*downsampleRates(r) + downsampleRates(r)/downsampleRates(r-1)/2;

        minOffsetX = reg_offsets(d,2)*downsampleRates(r) - downsampleRates(r)/downsampleRates(r-1)/2;
        maxOffsetX = reg_offsets(d,2)*downsampleRates(r) + downsampleRates(r)/downsampleRates(r-1)/2;
        
        [bestCorrY, bestCorrX] = corr2max(regImg, templateImg, minOffsetY, maxOffsetY, minOffsetX, maxOffsetX);

        regOffsetsResult(d,:) = [bestCorrY/downsampleRates(r), bestCorrX/downsampleRates(r)];
    end
    reg_offsets = regOffsetsResult;
end


disp(['Registration computed. Applying offsets to images.']);

% now we have the regOffsets; apply them to the images and output results.
output_images = zeros(size(input_images));
for d=1:depth
    img = input_images(:,:,d);
    shiftedY1 = 1+max(reg_offsets(d,1),0);
    shiftedY2 = sizeY+min(reg_offsets(d,1),0);
    shiftedX1 = 1+max(reg_offsets(d,2),0);
    shiftedX2 = sizeX+min(reg_offsets(d,2),0);
    
    subRegY1 = 1+max(-reg_offsets(d,1),0);
    subRegY2 = sizeY+min(-reg_offsets(d,1),0);
    subRegX1 = 1+max(-reg_offsets(d,2),0);
    subRegX2 = sizeX+min(-reg_offsets(d,2),0);
    
    shiftedImg = ones(sizeY,sizeX)*double(min(min(img)));
    shiftedImg(shiftedY1:shiftedY2,shiftedX1:shiftedX2) = img(subRegY1:subRegY2,subRegX1:subRegX2);
    output_images(:,:,d) = shiftedImg;
end



end

