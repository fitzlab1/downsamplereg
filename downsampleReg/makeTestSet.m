% Makes a set of simple images, jittered randomly.
% We can then feed these into downsampleReg so it can correct the jitter.

sizeX = 256;
sizeY = 256;
depth = 20;

template = uint16(zeros([sizeY, sizeX]));

template(sizeY/2, :) = 65535;
template(:, sizeX/2) = 65535;

imgStack = uint16(zeros([sizeY, sizeX, depth]));

offsets = zeros(depth,2);
for d=1:depth
    imgD = imgStack(:,:,d);
    
    if d==1
        % first image has no jitter
        offsetY = round(rand*10)-5;
        offsetX = round(rand*10)-5;
        offsets(d,:) = [offsetY,offsetX];
    else
        % generate random jitter
        offsetY = 0;
        offsetX = 0;
        if rand > 0.25
            offsetY = (round(rand*5)-3)*2;
        end
        if rand > 0.25
            offsetX = (round(rand*5)-3)*2;
        end
        offsets(d,:) = [offsetY, offsetX];
    end
    
    % draw white lines centered on jittered location
    for i=0:2
        imgD(:, sizeX/2+offsetX+i) = 65535;
        imgD(sizeY/2+offsetY+i, :) = 65535;
    end
    imgStack(:,:,d) = imgD;
    
end

writeTifs(imgStack, './input')