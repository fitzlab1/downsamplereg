function [ template_img ] = makeTemplate( input_images, method )
% Returns the image with most unique autocorrelation peak.
% 
% Method choices:
% 'first': Simply uses the first image of input_images.
% 'mean': Takes the average of all input images.
% 'registered_mean': Quickly registers the first several images and
% uses the mean the registered result.
% 
% Default method is 'registered_mean'.

[sizeX, sizeY, num_images] = size(input_images);

if strcmp(method, 'first')
    template_img = squeeze(input_images(:,:,1));
elseif strcmp(method, 'mean')
    template_img = uint16(mean(input_images, 3));
else
    % use 'registered_mean' method
    num_template_images = min(num_images, 100);
    template_stack = zeros([sizeX, sizeY, num_template_images], 'uint16');
    for n=1:num_template_images
        template_stack(:,:,n) = input_images(:,:,n);
    end
    
    % Registration of the template stack is based on the first image.
    disp('Performing initial registration to develop a good template.');
    template_stack_registered = register(template_stack, squeeze(input_images(:,:,1)));
    template_img = uint16(mean(template_stack_registered, 3));
    disp('Template ready!');
    disp('Registering full dataset to template.');
end


end

