function [ filtered ] = stdfilter( img )
% Apply local standard deviation filter to img.
% Effectively, this performs both edge detection and denoising in one shot.
% Assumes 16-bit input. Output is multiplied by 10 to better
% use the dynamic range.

% filter = ones(3); 
% filtered = uint16(stdfilt(img, filter) * 10);
filtered = img;

end

