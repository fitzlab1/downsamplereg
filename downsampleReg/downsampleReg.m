% DownsampleReg 
% Theo Walker
% Jan. 3 2014

% === Description === %

%This registers time-series data, removing motion artifacts. 
%It is assumed that your input directory
%contains a series of TIF images; an output directory of registered TIFs is
%produced.

% === Algorithm Explanation === %

%So, the naive way to do register a pair of images is to consider all 
%possible shifts of one image relative to the other, and find where they 
%correlate best. This algorithm is an improvement on that idea.

%The way this works is super simple. What we're going to do is downsample
%the image to something _very_ small, say 1/16 of the original size, and
%register all the slices using correlation.
%Then based on the 1/16 registration, we do a finer registration (1/8) that is
%constrained by the previous registration result, and so on. The constraining
%helps keep the algorithm from focusing on small details that don't matter 
%(i.e., noise) and also speeds up the runtime.

% === Input and Output === %

inputDir = './input';
outputDir = './registered';

%  === Code begins here! === %
disp('reading in data');
imgStack = readTifs(inputDir);

template = makeTemplate(imgStack, 'registered_mean');

[outStack, regOffsets] = register(imgStack, template);

disp(['Writing results to ' outputDir]);
writeTifs(outStack,outputDir);

disp(['Score before: ' num2str(regScore(imgStack)) ' after: ' num2str(regScore(outStack))]);

disp(['Done!']);
