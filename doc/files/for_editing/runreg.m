%% 
scoreDiffs = zeros(1,depth)

for i = 1:depth
    templateImg = imgStack(:,:,i);
    downsampleReg;
    
    scoreDiffs(i) = regScore(imgStack)-regScore(outStack)
end

%%
brightness = zeros(1,depth);
for i=1:depth
    brightness(i) = sum(sum(imgStack(:,:,i)));
end

%% 
edginess = zeros(1,depth);
for d=1:depth
    imgD = imgStack(:,:,d);

    grdX = abs(imgD(1:sizeY, 1:sizeX-1) - imgD(1:sizeY, 2:sizeX));
    grdY = abs(imgD(1:sizeY-1, 1:sizeX) - imgD(2:sizeY, 1:sizeX));
    
    edginess(d) = (sum(sum(grdX)) + sum(sum(grdY)));
end

edginess = edginess ./ brightness;

%%
flatness = zeros(1,depth);
for d=1:depth
    imgD = imgStack(:,:,d);

    bins = hist(double(reshape(imgD, [sizeX*sizeY,1])));
    
    
    
    flatness(d) = prod(bins)^0.1 / mean(bins) / 1000 - mean(edginess);
end

flatness = flatness ./ brightness;

%% 

sdScaled = (scoreDiffs - min(scoreDiffs)) / (max(scoreDiffs)-min(scoreDiffs));
brightScaled = (brightness - min(brightness)) / (max(brightness)-min(brightness));
edgeScaled = (edginess - min(edginess)) / (max(edginess)-min(edginess));
flatScaled = (flatness - min(flatness)) / (max(flatness)-min(flatness));

plot(sdScaled, 'k');
hold on;
plot(brightScaled, 'g');
plot(edgeScaled, 'r');
plot(flatScaled, 'b');


