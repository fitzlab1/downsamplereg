% DownsampleReg 
% Theo Walker
% Jan. 3 2014

% === Description === %

%This registers time-series data, removing motion artifacts. 
%It is assumed that your input directory
%contains a series of TIF images; an output directory of registered TIFs is
%produced.

% === Algorithm Explanation === %

%So, the naive way to do register a pair of images is to consider all 
%possible shifts of one image relative to the other, and find where they 
%correlate best. This algorithm is an improvement on that idea.

%The way this works is super simple. What we're going to do  is downsample
%the image to something _very_ small, say 1/16 of the original size, and
%register all the slices using correlation.
%Then based on the 1/16 registration, we do a finer registration (1/8) that is
%constrained by the previous registration result, and so on. The constraining
%helps keep the algorithm from focusing on small details that don't matter 
%(i.e., noise) and also speeds up the runtime.

% modified DanW 1/2015
% === Input and Output === %
for ii= 1:length(fileList);
     cd(strcat('f:\ForRongwen\',fileList(ii).name));
inputDir = cd;
outputDir = './Result';

% inputDir = './testSet';
% outputDir = './testSetResult';

% === Parameters === %

%Downsample rates: As described in algorithm. Examples follow.
downsampleRates = [1/4, 1/2, 1];
%
%downsampleRates = [1/4, 1/2, 1] is the default. 
%
%downsampleRates = [1/16] will do a registration _just_ based on the 16x
%downsampled image. It will run pretty quick, but the output will only be
%accurate to +/- 8 pixels; it won't do fine motion correction.

%e.g, if this is at 1/4 and your image is 512x512, this will fix movements 
%of up to 128 pixels.
%Makes runtime of the first iteration a little shorter to set this lower.
tic
%  === Code begins here! === %
disp('reading in data');
% imgStack = readTifs(inputDir);
files  = dir('*.tif');
imgStack = [];
for i = 1:length(files)
     [header,aout] =opentif(files(i).name); % use for SI5 and SI2015
%    [header,aout] = scim_openTif(files(i).name); use for SI4
    disp(strcat('Done reading in chunk',i))
    aout = squeeze(aout(:,:,1,:));
    imgStack = cat(3, imgStack, aout);
end
readTime = toc;

[sizeY sizeX depth] = size(imgStack);
regOffsets = zeros(depth,2); %stores the output of the registration (Y,X format)
template = int16(mean(imgStack(:,:,1:100),3)); %Everything gets registered to the mean image, change the template if registration is bad

tic;

%Use normxcorr2 on most downsampled (smallest) image
r=1;
disp(['registering images, iteration ' num2str(r)]);
downSizeY = sizeY*downsampleRates(r);
downSizeX = sizeX*downsampleRates(r);
templateImg = imresize(template, [downSizeY,downSizeX],'bilinear');
parfor d=1:depth
    regImg = imresize(imgStack(:,:,d), [downSizeY,downSizeX],'bilinear');
    
    cc = normxcorr2(regImg, templateImg);
    [max_cc, imax] = max(abs(cc(:)));
    [yPeak, xPeak] = ind2sub(size(cc), imax(1));
    corr_offset = [(yPeak-size(templateImg,1)), (xPeak-size(templateImg,2))];

    regOffsets(d,:) = [(yPeak-size(templateImg,1)), (xPeak-size(templateImg,2))] ./ downsampleRates(r);
end

%Use corr2max on successive pyramid levels
for r=2:length(downsampleRates)
    disp(['registering images, iteration ' num2str(r)]);
    
    downSizeY = sizeY*downsampleRates(r);
    downSizeX = sizeX*downsampleRates(r);
    templateImg = imresize(template, [downSizeY,downSizeX],'bilinear');
    
    regOffsetsResult = zeros(size(regOffsets));
    for d=1:depth
        regImg = imresize(imgStack(:,:,d), [downSizeY,downSizeX],'bilinear');
        
        %refine previous offset
        minOffsetY = regOffsets(d,1)*downsampleRates(r) - downsampleRates(r)/downsampleRates(r-1)/2;
        maxOffsetY = regOffsets(d,1)*downsampleRates(r) + downsampleRates(r)/downsampleRates(r-1)/2;

        minOffsetX = regOffsets(d,2)*downsampleRates(r) - downsampleRates(r)/downsampleRates(r-1)/2;
        maxOffsetX = regOffsets(d,2)*downsampleRates(r) + downsampleRates(r)/downsampleRates(r-1)/2;
        
        [bestCorrY, bestCorrX] = corr2max(regImg, int16(templateImg), minOffsetY, maxOffsetY, minOffsetX, maxOffsetX);

        regOffsetsResult(d,:) = [bestCorrY/downsampleRates(r), bestCorrX/downsampleRates(r)];
    end
    regOffsets = regOffsetsResult;
end

regTime = toc;

disp('Done!');
tic
% now we have the regOffsets; apply them to the images and output results.
for d=1:depth
    img = imgStack(:,:,d);
    shiftedY1 = 1+max(regOffsets(d,1),0);
    shiftedY2 = sizeY+min(regOffsets(d,1),0);
    shiftedX1 = 1+max(regOffsets(d,2),0);
    shiftedX2 = sizeX+min(regOffsets(d,2),0);
    
    subRegY1 = 1+max(-regOffsets(d,1),0);
    subRegY2 = sizeY+min(-regOffsets(d,1),0);
    subRegX1 = 1+max(-regOffsets(d,2),0);
    subRegX2 = sizeX+min(-regOffsets(d,2),0);
    
    shiftedImg = ones(sizeY,sizeX)*double(min(min(img)));
    shiftedImg(shiftedY1:shiftedY2,shiftedX1:shiftedX2) = img(subRegY1:subRegY2,subRegX1:subRegX2);
    imgStack(:,:,d) = shiftedImg;
end
writeTifChunks(imgStack,outputDir);
save('header.mat','header')
saveTime = toc;
disp(['Read time is ' num2str(readTime)]);
disp(['Reg time is ' num2str(regTime)]);
disp(['Save time is ' num2str(saveTime)]);
end;