function [ ] = writeTifChunks(tifSet, outDir)

if ~exist(outDir,'dir')
    mkdir(outDir);
end

%delete any tifs sitting in this dir
files = dir(outDir);
for i=1:length(files)
    if strendswith(files(i).name,'.tif')
        delete([outDir, '/', files(i).name]);        
    end
end

[x y z] = size(tifSet);
count = 0;
chunkNum = 1;
for i=1:z
    count = count+1;
    if count ==1001
        chunkNum = chunkNum+1;
        count = 1;
    end;
    %filename = [outDir, '/', filenames{i}];
    filename = [outDir '/' sprintf('%06i',chunkNum) '.tif'];
    %disp(filename)
    imwrite(uint16(squeeze(tifSet(:,:,i))),filename,'tif','writemode','append');
end
